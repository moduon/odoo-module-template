..
    Audience: end users, contributors, maintainers.

    Purpose: let them know limitations or potential future improvements.

    ⚠️ Cautions:

    - Enumerate known caveats and future potential improvements.
    - Help potential new contributors discover new features to implement.

    ⛔ REMOVE THIS FILE if the module is feature-complete or has no known issues.

* ...
