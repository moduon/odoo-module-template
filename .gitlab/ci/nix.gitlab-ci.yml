# Set up SSH access if available
# Docs: https://docs.gitlab.com/ee/ci/ssh_keys/
.ssh-setup: |
  mkdir -p ~/.ssh
  echo "$SSH_KNOWN_HOSTS" >> ~/.ssh/known_hosts

  if [ -n "$DEPLOY_SSH_KEY_PRIVATE_B64$DOWNLOAD_SSH_KEY_PRIVATE_B64" ]; then
    eval $(ssh-agent -s)
    echo "$DEPLOY_SSH_KEY_PRIVATE_B64" | base64 -d | ssh-add - || true
    echo "$DOWNLOAD_SSH_KEY_PRIVATE_B64" | base64 -d | ssh-add - || true
  fi

  chmod -R go= ~/.ssh

# Not used by default because there are better nix caching methods available,
# such as Cachix and Nixbuild. However, you can use it to extend any Nix job
# if you want to use the GitLab cache.
.nix:gitlab-cache:
  cache:
    - key:
        prefix: nix
        files:
          - "**.nix"
      paths:
        - .cache/nix/

.nix:
  tags:
    - nix # Use runner specialized in nix workloads by default
  variables:
    # Set these to use cachix; use $CACHIX_WATCHER prefix to commands that
    # should push to Cachix
    CACHIX_AUTH_TOKEN: ""
    CACHIX_CACHE_NAME: ""
    # Common nix config
    NIX_CONFIG: |
      accept-flake-config = true
      extra-experimental-features = nix-command flakes parse-toml-timestamps
      show-trace = true
    # Avoid polluting nix store paths with *.pyc files
    PYTHONDONTWRITEBYTECODE: 1
    # Add raw as known hosts; generate with ssh-keyscan
    SSH_KNOWN_HOSTS: ""
    # Make general cache local
    XDG_CACHE_HOME: $CI_PROJECT_DIR/.cache
  before_script:
    - echo -e "\e[0Ksection_start:`date +%s`:nix_setup[collapsed=true]\r\e[0KSetting up
      nix environment"

    # Let scripts know current system
    - export NIX_SYSTEM=$(nix-instantiate --eval --expr 'builtins.currentSystem')

    # Install cachix if needed
    - |
      if [ -n "$CACHIX_AUTH_TOKEN" -a -n "$CACHIX_CACHE_NAME" ]; then
        cachix --version || nix-env -f '<nixpkgs>' -iA cachix
        export CACHIX_WATCHER="cachix watch-exec $CACHIX_CACHE_NAME --"
      else
        export CACHIX_WATCHER=""
      fi

    - !reference [.ssh-setup]

    - echo -e "\e[0Ksection_end:`date +%s`:nix_setup\r\e[0K"

.nix:nix2container:build+push:
  extends: .nix
  variables:
    IMAGE_PACKAGE: "image"
    # Pushed image name and tag to registry
    IMAGE_NAME: $CI_REGISTRY_IMAGE
    IMAGE_TAG: latest
  script:
    - $CACHIX_WATCHER nix run ".#$IMAGE_PACKAGE.copyTo" -- --dest-creds
      "$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD" "docker://$IMAGE_NAME:$IMAGE_TAG"

.nix:image:build+push:
  extends: .nix:nix2container:build+push
  variables:
    # Arguments passed to nix build
    BUILD_ARGS: ".#$IMAGE_PACKAGE"
    # Pushed image name and tag to registry
    IMAGE_NAME: $CI_REGISTRY_IMAGE
    IMAGE_TAG: latest
  stage: build
  before_script:
    - !reference [.nix, before_script]
    - skopeo --version || nix-env -f '<nixpkgs>' -iA skopeo
    - echo WARNING migrate to .nix:nix2container:build+push which is much better
  script:
    # Build image
    - $CACHIX_WATCHER nix build $BUILD_ARGS

    # Push image to registry
    - skopeo --insecure-policy copy --dest-creds $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD
      docker-archive:result "docker://$IMAGE_NAME:$IMAGE_TAG"

.nix:image-stream:build+push:
  extends: .nix:image:build+push
  variables:
    # This should be an app output, to make it executable
    BUILD_ARGS: .#imageStream
  script:
    # Build streamed image and push to registry
    - $CACHIX_WATCHER nix run $BUILD_ARGS | gzip --fast | skopeo --insecure-policy copy
      --dest-creds $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD docker-archive:/dev/stdin
      "docker://$IMAGE_NAME:$IMAGE_TAG"

.nix:direnv:
  extends: .nix
  before_script:
    - !reference [.nix, before_script]
    - echo -e "\e[0Ksection_start:`date +%s`:direnv_setup[collapsed=true]\r\e[0KSetting
      up direnv"
    - direnv --version || nix-env -f '<nixpkgs>' -iA direnv
    - $CACHIX_WATCHER direnv allow
    - eval "$($CACHIX_WATCHER direnv export bash)"
    - echo -e "\e[0Ksection_end:`date +%s`:direnv_setup\r\e[0K"
