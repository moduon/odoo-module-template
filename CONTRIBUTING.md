# Develop online

Start hacking in no time, just click here:

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/moduon/odoo-module-template)

You will see a terminal setting up the project. Let it do its job. You can hack in the
mean time.

# Develop locally

We use Precommix, so
[follow upstream quick startguide](https://gitlab.com/moduon/precommix#quick-start).
