<!--
    Audience: Everybody.

    Purpose: Credit whoever participated in the module development.
-->

-   Firstname Lastname \<email.address@example.org> (optional company website url)
-   Second Person \<second.person@example.org> (optional company website url)
-   Moduon Employee ([Moduon](https://www.moduon.team/))
