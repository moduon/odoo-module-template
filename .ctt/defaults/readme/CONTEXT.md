<!--
    Audience: end users.

    Purpose: help them evaluate if this module is useful in their context:

    - Why this module exists.
    - Business requirement that generated the need to develop this module.
    - Point to other related modules.
    - Explain how those other modules can complement this one.
-->

This module was developed because ...

It will be useful for you if ...

If you need this module for those reasons, these might interest you too:

-   ...
