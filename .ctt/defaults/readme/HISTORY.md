<!--
    Audience: Odoo users and integrators.

    Purpose: help them know what changed in this module over time.

    ⚠️ Cautions:

    - DO NOT include purely technical changes such as code refactoring.
    - This file may contain ONE level of section titles, underlined
      with the ~ (tilde) character. Other section markers are
      forbidden and will likely break the structure of the README.rst
      or other documents where this fragment is included.

    ⛔ REMOVE THIS FILE if you won't maintain this file updated. It is better to
    have no changelog than to have it unmaintained. In such case, you probably
    want to remove ./newsfragments/ too.
-->

## 11.0.x.y.z (YYYY-MM-DD)

-   [BREAKING] Breaking changes come first.
    ([#70](https://github.com/OCA/repo/issues/70))
-   [ADD] New feature. ([#74](https://github.com/OCA/repo/issues/74))
-   [FIX] Correct this. ([#71](https://github.com/OCA/repo/issues/71))

## 11.0.x.y.z (YYYY-MM-DD)

-   ...
