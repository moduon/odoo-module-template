<!--
    Audience: users that will configure Odoo to make this module work.

    Purpose: help them configure Odoo appropriately.

    ⚠️ Cautions:

    - Do not assume the user knows how to find the menus. Guide them there.

    ⛔ REMOVE THIS FILE if the module works out of the box and there is nothing to configure.
-->

To configure this module, you need to:

1. Go to ...

![alternative description](../static/description/image.png)
